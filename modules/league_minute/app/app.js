//Define an angular module for our app
var Minutes = angular.module('MinuteMinute', ['xeditable']);

jQuery(document).ready(function() {

  /**
   * We are initialization the angular app by hand and not with the Automatic
   * Initialization.
   * The reason for this is that we are loading multiple angular apps on one
   * page and the Automatic Initialization can only handle 1 app per page.
   *
   * @link http://docs.angularjs.org/guide/bootstrap
   */
  angular.bootstrap(document.getElementById('IdMinute'),['MinuteMinute']);
});

Minutes.run(function(editableOptions) {
  editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
});

Minutes.controller('MinuteController', function($scope, $http) {
  $scope.gameInit = function(gid) {
     $scope.game = gid;
     getMinutes(gid);
  }

  $scope.addTask = 11;
  $scope.addNewClicked = false;
  $scope.MinuteTemp = {};
  $scope.MinuteTemp.twitter = 0;
  $scope.MinuteTemp.is_vine = 0;
  getTypesMinutes();
  getTimesMinutes(); 

  function getMinutes(game){
    $http.post("/league_game/minutes/get/"+game).success(function(data){
          $scope.minutes = data;
          $scope.game = game;
    });
  };
  function getTypesMinutes(){  
    $http.post("/league_game/minutes/type/json").success(function(data){
          $scope.TypesMinutes = data;
    });
  };
  function getTimesMinutes(){  
    $http.post("/league_game/minutes/time/json").success(function(data){
          $scope.TimesMinutes = data;
    });
  };
  $scope.ShowVine = function (MinuteTemp) {
    MinuteTemp.is_vine = MinuteTemp.is_vine == 0 ? 1 : 0;
  };
  $scope.toggleTwitter = function(minute) {
    if(minute.is_twitter == 1){minute.is_twitter=0;}else{minute.is_twitter=1;}
      $http.post('/league_game/minutes/update', {minute:minute,field:'is_twitter'}).success(function(data){
      });
  };
  $scope.updateMinute = function(minute, field) {
      $http.post('/league_game/minutes/update', {minute:minute,field:field}).success(function(data){
        if(field == 'time_id') {
           getMinutes($scope.game);
        }
      });
  };
  $scope.saveMinute = function(minute_temp, game) {
      $http.post('/league_game/minutes/save', {minute:minute_temp,game:game}).success(function(data){
        getMinutes($scope.game);
        $scope.addNewClicked = false;
      });
  };
  $scope.deleteMinute= function (minute) {
    if(confirm("Are you sure to delete this minute?")){
    $http.post("/league_game/minutes/delete/"+minute).success(function(data){
        getMinutes($scope.game);
      });
    }
   };
});