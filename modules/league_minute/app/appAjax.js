//Define an angular module for our app
var Minutes = angular.module('MinuteMinute', ['720kb.socialshare']);

jQuery(document).ready(function() {

  /**
   * We are initialization the angular app by hand and not with the Automatic
   * Initialization.
   * The reason for this is that we are loading multiple angular apps on one
   * page and the Automatic Initialization can only handle 1 app per page.
   *
   * @link http://docs.angularjs.org/guide/bootstrap
   */
  angular.bootstrap(document.getElementById('IdMinute'),['MinuteMinute']);
});
Minutes.controller('MinuteController', function($scope, $http, $interval) {
  var power = Drupal.settings.league_minute.power;
  var cache = Drupal.settings.league_minute.cache;
  var game = Drupal.settings.league_minute.game;
  var d = 0;
  getMinutes(game,d);
  if(power == 1) {
    var interval = Drupal.settings.league_minute.interval;
    $interval(function() {
      d = Math.round(Date.now()/cache);
      getMinutes(game,d);
    }, interval);
  }
  function getMinutes(game,d){
    $http.get("/league_game/minutes/get/"+game+"?t=" + d).success(function(data){
          $scope.minutes = data;
    });
  };
});