<?php

/**
 * @file
 * Sets up the base table for our entity and a table to store information about
 * the entity types.
 */

/**
 * Default settings storage.
 */
function _league_minute_defaults() {
  return array(
    'minute' => array(
      'filter_format' => 'text_plain',
    ),
    'ajax' => array(
      'active' => 0,
      'interval' => 60000,
    ),
  );
}

/*
 + implements hook_install
 *  */
function league_minute_install() {
  variable_set('league_minute_settings', _league_minute_defaults());
  $fields = array(
    'label' => 'None',
    'class_icon' => 'none',
  );
  db_insert('league_minute_type')->fields($fields)->execute();
  $fields = array(
    'label' => 'Goal',
    'class_icon' => 'goal',
  );
  db_insert('league_minute_type')->fields($fields)->execute();
  $fields = array(
    'label' => 'Corner',
    'class_icon' => 'corner',
  );
  db_insert('league_minute_type')->fields($fields)->execute();
  $fields = array(
    'label' => 'Substitution',
    'class_icon' => 'substitution',
  );
  db_insert('league_minute_type')->fields($fields)->execute();
  $fields = array(
    'label' => 'Whistle',
    'class_icon' => 'whistle',
  );
  db_insert('league_minute_type')->fields($fields)->execute();
  $fields = array(
    'label' => 'Red Card',
    'class_icon' => 'redcard',
  );
  db_insert('league_minute_type')->fields($fields)->execute();
  $fields = array(
    'label' => 'Yelow Card',
    'class_icon' => 'yellowcard',
  );
  db_insert('league_minute_type')->fields($fields)->execute();
  $fields = array(
    'label' => 'Clock',
    'class_icon' => 'clock',
  );
  db_insert('league_minute_type')->fields($fields)->execute();

  /*Insert TIME*/
  $fields = array(
    'label' => 'First time',
  );
  db_insert('league_minute_time')->fields($fields)->execute();
  $fields = array(
    'label' => 'Second time',
  );
  db_insert('league_minute_time')->fields($fields)->execute();
  $fields = array(
    'label' => 'First additional',
  );
  db_insert('league_minute_time')->fields($fields)->execute();
  $fields = array(
    'label' => 'Second additional',
  );
  db_insert('league_minute_time')->fields($fields)->execute();
  $fields = array(
    'label' => 'Penalties',
  );
  db_insert('league_minute_time')->fields($fields)->execute();
}

/**
 * Implements hook_schema().
 */
function league_minute_schema() {
  $schema = array();

  $schema['league_minute'] = array(
    'description' => 'The base table for league_minute entities.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique league_minute type identifier.',
      ),
      'game_id' => array(
        'description' => 'id the page',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'time_id' => array(
        'description' => 'Time',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'minute' => array(
        'description' => 'Minute.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'type_id' => array(
        'description' => 'Type minute',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'comment' => array(
        'description' => 'Comment of the minute to minute',
        'type' => 'varchar', 
		'length' => 500,
        'not null' => FALSE,
        //'default' => '',
      ),     
      'format' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'description' => 'The {filter_format}.format of the comment.',
      ),
      'is_twitter' => array(
        'description' => 'Publish in twitter', 
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'vine' => array(
        'type' => 'varchar',
		'length' => 255,
        'not null' => FALSE,
		'description' => 'Video vine', 
      ),
      'active' => array(
        'description' => 'Active',
        'type' => 'int',
        'default' => 1,
      ),
    ),
    'foreign keys' => array(
      'minute_type' => array(
        'table' => 'league_minute_type',
        'columns' => array('type_id' => 'id'),
       ),
      'minute_time' => array(
        'table' => 'league_minute_time',
        'columns' => array('time_id' => 'id'),
       ),
    ),
   'primary key' => array('id'),
  );

  $schema['league_minute_type'] = array(
    'description' => 'The types of league_minutes.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique league_event type identifier.',
      ),
      'label' => array(
        'description' => 'The human-readable name of this league_minute type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'class_icon' => array(
        'description' => 'Class of the icons.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'active' => array(
        'description' => 'Active',
        'type' => 'int',
        'default' => 1,
      ),
    ),
    'primary key' => array('id'),
  );

  $schema['league_minute_time'] = array(
    'description' => 'The types of league_minutes.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique league_event type identifier.',
      ),
      'label' => array(
        'description' => 'The human-readable name of this league_minute type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'active' => array(
        'description' => 'Active',
        'type' => 'int',
        'default' => 1,
      ),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}

/**
 * Implementation of hook_uninstall().
 * Only clears our variables, so a fresh installation can repopulate them.
 */
function league_minute_uninstall() {
  // Settings.
  variable_del('league_minute_settings');
}