<?php

/**
 * @file
 * A basic template for league_game entities
 *
 * Available variables:
 * - $items: An array of comment items. Use render($content) to print them all, or
 * - $game: The game id
 * - $label: The field label
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<style type="text/css">
.text-area textarea{width: 441px !important;}
.icon {
  float: left;
  width: 35px;
  height: 35px;
  border-right: 1px solid #d1d1d1;
  background-color: #ededed;
}
</style>
<div class="widget-box" id="IdMinute" ng-controller="MinuteController">
  <div class="row">
    <table class="table table-striped">
      <thead>
        <tr>
          <th><?php print t('');?></th>
          <th><?php print t('Minute');?></th>
          <th><?php print t('Comment');?></th>
          <th><?php print t('Twitter');?></th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="minute in minutes">
          <td>
            <div class="icon {{minute.class_icon}}"></div>
          </td>
          <td>
            <div class="minute">{{minute.minute}}</div>
          </td>
          <td>
            {{minute.comment}}
          </td>
          <td>
            <div class="widget-body" ng-if="minute.is_twitter==1">
              <a class="twitter" href="#" socialshare socialshare-provider="twitter"
              socialshare-url="{{minute.vine}}"
              socialshare-text="{{minute.comment}}">
              Twitter
              </a>
              <!-- https://vine.co/v/eMDInDlaeZh -->
              </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
<!--
  <div class="widget-body ">
  <a href="#" socialshare socialshare-provider="twitter"
  socialshare-url="https://vine.co/v/eMDInDlaeZh"
  socialshare-text="GOOOLLLL"
  socialshare-hashtags="woa, wonderful, interesting">
  Twitter
  </a>
  </div>-->

</div>
