<?php

/**
 * @file
 * A basic template for league_game entities
 *
 * Available variables:
 * - $items: An array of comment items. Use render($content) to print them all, or
 * - $game: The game id
 * - $label: The field label
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<style type="text/css">
.text-area textarea{width: 441px !important;}
</style>

<?php print render($form);?>

<div class="widget-box" id="IdMinute" ng-controller="MinuteController" ng-init="gameInit(<?php print $game;?>)">
  <div class="row">
    <div class="col-sm-3">
      <button ng-click="addNewClicked=!addNewClicked;" class="btn btn-sm btn-danger header-elements-margin">
        <i class="glyphicon  glyphicon-plus"></i>Add new minute
      </button>
    </div>
  </div>
  <div ng-if="addNewClicked==true" id="newTaskForm" class="add-task">
  <form>
    <div class="form-actions">
      <div class="input-group">
        <div class="col-sm-3">
          <div class="">
            <label>Minute</label>
            <input type="number"  class="form-control" name="minute" ng-model="MinuteTemp.minute" placeholder="Enter minute" min="0" max="140" required>
          </div>
          <div class="">
            <label>Time</label>
            <select ng-model="MinuteTemp.time" ng-init="MinuteTemp.time = 0 || TimesMinutes[0].value" class="form-control" ng-options="option.value as option.name for option in TimesMinutes" required></select>
          </div>
          <div class="">
            <label>Icon</label>
              <select ng-model="MinuteTemp.type" ng-init="MinuteTemp.type = 0 || TypesMinutes[0].value" class="form-control" ng-options="option.value as option.name for option in TypesMinutes" required></select>
          </div>
        </div>
        <div class="col-sm-9">
          <label>Comment</label>
          <textarea class="form-control" rows="7" ng-model="MinuteTemp.comment" placeholder="Enter comment"></textarea>
        </div>
        <div class="col-sm-3">
          <div class="checkbox">
            <label><input value="{{MinuteTemp.twitter}}" type="checkbox" >Twitter</label>
          </div>
          <div class="checkbox">
            <label><input value="{{MinuteTemp.is_vine}}" ng-checked="MinuteTemp.is_vine==1" type="checkbox" ng-click="ShowVine(MinuteTemp)"> <?php print t('Send video vine'); ?></label>
          </div>
        </div>
        <div class="col-sm-9">
          <div ng-if="MinuteTemp.is_vine == 1">
            <label>URL Vine</label>
            <input type="text" class="form-control" name="vine" ng-model="MinuteTemp.vine" placeholder="Insert url vine" required>
          </div>
        </div>
        <div class="input-group-btn">
          <button class="btn btn-default" type="submit" ng-click="saveMinute(MinuteTemp,1)"><i class="glyphicon glyphicon-plus"></i> Save New Minute</button>
        </div>
      </div>
    </div>
  </form>
 </div>
  <div class="row">
    <table class="table table-striped">
      <thead>
        <tr>
          <th><?php print t('Minute');?></th>
          <th><?php print t('Comment');?></th>
          <th><?php print t('Icon');?></th>
          <th><?php print t('Time');?></th>
          <th><?php print t('Vine');?></th>
          <th><?php print t('Twitter');?></th>
          <th><?php print t('Delete');?></th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="minute in minutes">
          <td>
            <a href="#" onaftersave="updateMinute(minute,'minute')" editable-text="minute.minute">{{minute.minute}}</a>
          </td>
          <td>
            <div class="text-area">
             <a href="#" onaftersave="updateMinute(minute,'comment')" editable-textarea="minute.comment">
                 {{minute.comment}}
                </a>
            </div>
          </td>
          <td>
            <select ng-model="minute.c_id" ng-change="updateMinute(minute,'type_id')" class="form-control" ng-init="minute.c_id" ng-options="option.value as option.name for option in TypesMinutes" required></select>
          </td>
          <td>
            <select ng-model="minute.t_id" ng-change="updateMinute(minute,'time_id')" ng-init="minute.c_id" class="form-control" ng-options="option.value as option.name for option in TimesMinutes" required></select>
          </td>
          <td>
             <a href="#" ng-if="minute.vine" onaftersave="updateMinute(minute,'vine')" editable-text="minute.vine">{{minute.vine}}</a>
             <a href="#" ng-if="minute.vine == NULL || minute.vine == ''" onaftersave="updateMinute(minute,'vine')" editable-text="minute.vine">__</a>
          </td>
          <td>
            <input type="checkbox" value="{{minute.is_twitter}}" ng-checked="minute.is_twitter==1" ng-click="toggleTwitter(minute)"/>
          </td>
          <td>
            <a href="" ng-click="deleteMinute(minute.id)">
              <i class="glyphicon glyphicon-trash"></i>
            </a>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
