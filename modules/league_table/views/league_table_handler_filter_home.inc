<?php
/**
* @file
* Provide node nid argument handler.
*/
/**
* Argument handler to accept a home/away filter.
*/
class league_table_handler_filter_home extends views_handler_filter_equality {
  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);
    $form['value'] = array( 
// TODO: Change to autocomplete
      '#type' => 'select',
      '#title' => t('Home/Away'),
      '#options' => array( 
        0 => t('None'),
        1 => t('Home'),
        2 => t('Away'),
      ),
      '#default_value' => $this->value,
      '#required' => FALSE,
    );
  }
}
