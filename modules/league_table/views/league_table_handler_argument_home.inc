<?php
/**
* @file
* Provide node nid argument handler.
*/
/**
* Argument handler to accept local/away.
*/
class league_table_handler_argument_home extends views_handler_argument_numeric {
/**
* Override the behavior of title().
*/
  function title_query() {
    $titles = array();
    $titles[0] = 'None';
    $titles[1] = 'Home';
    $titles[2] = 'Away';
    return array($titles[$this->value]);
  }
} 